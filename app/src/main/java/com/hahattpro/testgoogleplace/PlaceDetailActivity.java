package com.hahattpro.testgoogleplace;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.location.places.ui.PlacePicker;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class PlaceDetailActivity extends AppCompatActivity {

    PlaceDetail placeDetail;

    //view
    LinearLayout linearLayout;
    TextView textView;

    //Button
    Button phone_bt;
    Button website_bt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_detail);

        linearLayout = (LinearLayout) findViewById(R.id.placeDetailLayout_id);
        textView = (TextView) findViewById(R.id.placeInfoTextview_id);
        phone_bt = (Button) findViewById(R.id.phoneButton_id);
        website_bt = (Button) findViewById(R.id.gotoWebsiteButton_id);

        new LoadPlaceData().execute();
    }

    private void getPlaceDetailFromIntent(){
        Bundle data = getIntent().getExtras();
        String placeID = data.getString(getResources().getString(R.string.put_extra_placeID));
        String place_json = PlaceDetail.requestPlaceJson(placeID);
        placeDetail = new PlaceDetail(place_json);
    }

    private void showInfomation(){
        String result = placeDetail.getInfomationString();
        textView.setText(result);
        if (placeDetail.hasAnyPhotos())
        {
            //TODO: show PHOTO banner

            //put on photo
            ArrayList<Photo> photos = placeDetail.getPhotos();
            for (int i = 0 ;i<photos.size();i++)
            {
                new LoadPhotos().execute(photos.get(i));
            }
        }

        if (placeDetail.hasPhone())
        {
            phone_bt.setVisibility(View.VISIBLE);
            phone_bt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String phone = placeDetail.getInternational_phone();
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:"+phone));
                    startActivity(callIntent);
                }
            });
        }

        if (placeDetail.hasWebSite()){
            website_bt.setVisibility(View.VISIBLE);
            website_bt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(PlaceDetailActivity.this,BrowserActivity.class);
                    intent.putExtra(getResources().getString(R.string.put_extra_websie),placeDetail.getWebsite());
                    startActivity(intent);
                }
            });
        }
    }

    private class LoadPlaceData extends AsyncTask<Void,Void,Void>{
        @Override
        protected Void doInBackground(Void... params) {
            getPlaceDetailFromIntent();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            showInfomation();
        }
    }

    private class LoadPhotos extends AsyncTask<Photo,Void,Bitmap>{
        @Override
        protected Bitmap doInBackground(Photo... params) {
            Bitmap result = params[0].requestBitmap();
            return result;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            ImageView imageView = new ImageView(PlaceDetailActivity.this);
            imageView.setImageBitmap(bitmap);
            linearLayout.addView(imageView);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_place_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
