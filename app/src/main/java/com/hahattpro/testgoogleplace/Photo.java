package com.hahattpro.testgoogleplace;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Thien on 8/1/2015.
 */
public class Photo {
    String photo_reference;

    public Photo(String photo_reference) {
        this.photo_reference = photo_reference;
    }

    public Photo() {
    }


    public String getPhoto_reference() {
        return photo_reference;
    }

    public void setPhoto_reference(String photo_reference) {
        this.photo_reference = photo_reference;
    }

    public Bitmap requestBitmap(){
        Bitmap result = null;
        String PLACE_DETAIL_BASE_URL = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400";
        String  PHOTO_REF=  "photoreference";
        String KEY = "key";

        Uri Uri_builder= Uri.parse(PLACE_DETAIL_BASE_URL).buildUpon()
                .appendQueryParameter(PHOTO_REF, photo_reference)
                .appendQueryParameter(KEY, "AIzaSyB3TrfG4ekznxlcM4LNv6avi4cVuu3UfXc").build();

        URL url = null;
        HttpURLConnection httpURLConnection = null;
        try {

            url = new URL(Uri_builder.toString());
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.connect();

            Log.v("URL", url.toString());//Log url

            // Read the input stream
            InputStream inputStream = httpURLConnection.getInputStream();
            result = BitmapFactory.decodeStream(inputStream);
        }
        catch (MalformedURLException e)
        {
            e.getStackTrace();
        }
        catch (IOException e)
        {
            e.getStackTrace();
        }
        return result;
    }
}
