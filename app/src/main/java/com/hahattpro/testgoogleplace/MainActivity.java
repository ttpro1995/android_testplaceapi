package com.hahattpro.testgoogleplace;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.hahattpro.meowdebughelper.Mailer;
import com.hahattpro.meowdebughelper.SaveFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends Activity  {

    private GoogleApiClient mGoogleApiClient;
    String LOG_TAG = MainActivity.class.getSimpleName();
    private GoogleApiClient.ConnectionCallbacks connectionCallbacks;
    private GoogleApiClient.OnConnectionFailedListener failedListener;

    private String server_API_key ="AIzaSyB3TrfG4ekznxlcM4LNv6avi4cVuu3UfXc";

    //request code
    int PLACE_PICKER_REQUEST = 101;

    //Button
    Button showPicker;
    Button meow;

    //textView
    TextView textView;

    LinearLayout linearLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout_id);

        createGoogleAPIcallback();
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(connectionCallbacks)
                .addOnConnectionFailedListener(failedListener)
                .build();

        createAllButton();

    }
    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    private void createAllButton(){
        showPicker = (Button) findViewById(R.id.showPickerbt_id);
        showPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                Context context = getApplicationContext();
                try {
                    startActivityForResult(builder.build(context), PLACE_PICKER_REQUEST);
                } catch (Exception e) {
                    Log.e(LOG_TAG, "meow ? something meow ?");
                    e.printStackTrace();
                }
            }
        });
        meow = (Button) findViewById(R.id.meow1);

    }

    private String showPlace(Place mPlace){
        String result = null;
        result =   mPlace.getName().toString() +"\n"+
                "Address " + mPlace.getAddress()+"\n"+
                "Price Level "+mPlace.getPriceLevel()+"\n"+
                "Phone number " + mPlace.getPhoneNumber()+ "\n"+
                "Place id " + mPlace.getId();

        Uri uri =  mPlace.getWebsiteUri();
        if (uri!= null) {
           result = result + "Website " + uri.toString()+"\n";
        }


        textView = (TextView) findViewById(R.id.placeInfoTextview_id);
        textView.setText(result);

        return result;
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                final Place place = PlacePicker.getPlace(data, this);
                //PlacePicker.getLatLngBounds(data);
                String attribution = PlacePicker.getAttributions(data);
                Log.i(LOG_TAG, "attribution = " + attribution);
                String toastMsg = String.format("Place: %s", place.getName());
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
                Log.i(LOG_TAG, "Meow " + toastMsg);
                goToPlaceDetailActivity(place.getId());

                //showPlace(place);
//                final Thread RequestPlaceDetailThread = new Thread() {
//                    @Override
//                    public void run() {
//                        super.run();
//                        String place_json = PlaceDetail.requestPlaceJson(place.getId());
//                        goToPlaceDetailActivity(place_json);
//                        SaveFile saveFile = new SaveFile(place.getName() + ".json", place_json, MainActivity.this);
//                        Mailer mailer = new Mailer(MainActivity.this);
//                        String email = "testing.ttpro1995@yahoo.com.vn";
//                        File file = saveFile.getFile();
//                        mailer.SendMail(email,"chu nhat "+file.getName()+" "+Long.toString(System.currentTimeMillis()),"Keep calm and Meow On",file);
//
//                    }
//                };
//                RequestPlaceDetailThread.start();
            }
        }
    }


    private void goToPlaceDetailActivity(String placeID){
        Intent intent = new Intent(MainActivity.this,PlaceDetailActivity.class);
        intent.putExtra(getResources().getString(R.string.put_extra_placeID),placeID);
        startActivity(intent);
    }

    private void displayImage(final PlaceDetail placeDetail){
        // no photo, quit
        if (!placeDetail.hasAnyPhotos())
            return;
        final ArrayList<Photo> photos = placeDetail.getPhotos();
        for (int i =0;i<photos.size();i++){
            new downloadBitmapAsync().execute(photos.get(i));
        }
    }

    private class downloadBitmapAsync extends AsyncTask<Photo,Void,Bitmap>{
        @Override
        protected Bitmap doInBackground(Photo... params) {
            Bitmap bitmap = params[0].requestBitmap();
            return bitmap;
        }



        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            ImageView imageView = new ImageView(MainActivity.this);
            imageView.setImageBitmap(bitmap);
            linearLayout.addView(imageView);
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void createGoogleAPIcallback(){
        connectionCallbacks = new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(Bundle bundle) {
                Log.i(LOG_TAG,"onConnected");
            }

            @Override
            public void onConnectionSuspended(int i) {
                Log.i(LOG_TAG,"onConnectionSuspended");
            }
        };
        failedListener = new GoogleApiClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(ConnectionResult connectionResult) {
                Log.i(LOG_TAG,"onConnectionFailed");
            }
        };
    }



}
